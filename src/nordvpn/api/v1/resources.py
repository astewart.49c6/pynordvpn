import logging
import json
from datetime import datetime
from ipaddress import IPv4Address
from functools import partialmethod, partial, total_ordering
from pprint import pformat

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class ResourceFilter():

    FILTER_TYPES = [
        'country_id',
        'servers_groups',
        'servers_technologies',
    ]

    def __init__(self, filter_type, value):
        self.type = filter_type
        self.value = value

    def __repr__(self):
        return 'filters[%s]=%s' % (self.type, self.value)

    @staticmethod
    def from_APIResource(resource):
        if isinstance(resource, Country) or isinstance(resource, Server.Location.Country):
            return ResourceFilter('country_id', resource.id)
        elif isinstance(resource, Server.Group):
            return ResourceFilter('servers_groups', resource.id)
        elif isinstance(resource, Technology):
            return ResourceFilter('servers_technologies', resource.id)
        else:
            raise ValueError('APIResource type %s is not supported.' % type(resource))

    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, value):
        if value not in self.FILTER_TYPES:
            raise ValueError('Invalid filter type: %s' % str(value))
        self._type = value

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, value):
        self._value = value


@total_ordering
class NordAPIInt():

    def __init__(self, value):
        self._int = int(value) if value is not None else None

    def __eq__(self, other):
        try:
            return self._int.__eq__(other._int)
        except AttributeError:
            return False

    def __ne__(self, other):
        try:
            return self._int.__ne__(other._int)
        except AttributeError:
            return False

    def __gt__(self, other):
        try:
            return self._int.__gt__(other._int)
        except AttributeError:
            return False

    def __repr__(self):
        return self._int.__repr__()

    @property
    def value(self):
        return self._int

    @value.setter
    def value(self, new_value):
        self._int = new_value


@total_ordering
class NordAPIDateTime():

    NORDAPI_DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'
    """(str): ISO 8601 datetime string like: 2020-12-20 12:15:03"""

    def __init__(self, nord_datetime: datetime):
        if isinstance(nord_datetime, NordAPIDateTime):
            self._dt = nord_datetime._dt
        else:
            self._dt = datetime.fromisoformat(str(nord_datetime))

    def __eq__(self, other):
        try:
            return self._dt.__eq__(other._dt)
        except AttributeError:
            return False

    def __ne__(self, other):
        try:
            return self._dt.__ne__(other._dt)
        except AttributeError:
            return False

    def __gt__(self, other):
        try:
            return self._dt.__gt__(other._dt)
        except AttributeError:
            return False

    def __repr__(self):
        return self._dt.isoformat(sep=' ', timespec='seconds')

    @property
    def datetime(self) -> datetime:
        return self._dt



class NordAPIIPV4(IPv4Address):
    """The NordVPN API representation of an IPv4 address."""
    pass


class APIResource():
    """Base class for all complex NordVPN API resources.

    This class roughly corresponds to the various types of JSON collections
    returned by the REST API.
    """

    autoproperties = {
        'id': int,
        'created_at': NordAPIDateTime,
        'updated_at': NordAPIDateTime,
    }

    def __init__(self, raw: str):
        """
        Args:
            raw: the raw JSON string from which to create the object.
        """
        if not isinstance(raw, dict) or not raw:
            raise ValueError('raw parameter must be a populated dictionary of JSON values.')

        self._properties = set()
        self.raw = raw

        failed = False
        for property, cast in self.autoproperties.items():
            if property not in raw.keys():
                logger.error('%s raw JSON missing required property %s <%s>' %
                    (self.__class__.__name__, property, cast))
                logger.error(pformat(raw))
                failed = True
            else:
                setattr(self, property, raw[property])
        if failed:
            raise ValueError('raw parameter missing required properties.')

    def __setattr__(self, name, value):
        if name == '_properties':
            self.__dict__['_properties'] = value
            return
        # Declared autoproperties are treated specially. They may not be set to
        # None, and must be passed through their autoproperty constructor,
        # unless they are already an APIResource type.
        elif name in self.autoproperties.keys():
            try:
                if isinstance(value, APIResource):
                    self.__dict__[name] = value
                else:
                    self.__dict__[name] = self.autoproperties[name](value)
            except TypeError as e:
                raise ValueError(e)
            else:
                self.__dict__['_properties'].add(name)
                return

        # Otherwise, this is just a generic python attribute-assignment.
        self.__dict__[name] = value

    def __repr__(self):
        prop_names = set(self.autoproperties.keys())
        for property, value in self.__dict__.items():
            if isinstance(value, APIResource):
                prop_names.add(property)
        rep_props = []
        for name in sorted(prop_names):
            rep_props.append('%s="%s"' % (name, str(getattr(self, name))))

        return '%s(%s)' % (self.__class__.__name__, ', '.join(rep_props))

    def __str__(self):
        return self.__repr__() + '\n'

    def add_resource_collection(self, name, collection):
        setattr(self, name, collection)
        self._properties.add(name)

    @property
    def property_names(self):
        return self._properties

    def json(self):
        raise NotImplementedError()


class City(APIResource):
    """An API City resource."""

    autoproperties = {
        'id': int,
        'dns_name': str,
        'name': str,
        'latitude': float,
        'longitude': float,
        'hub_score': NordAPIInt,
    }


class Country(APIResource):
    """An API Country resource - might contain Cities."""

    autoproperties = {
        'id': int,
        'code': str,
        'name': str,
    }

    def __init__(self, raw: str):
        """
        Args:
            raw: the raw JSON string from which to create the object.
        """
        super().__init__(raw)
        try:
            self.cities = [City(craw) for craw in self.raw['cities']]
        except KeyError:
            pass


class Server(APIResource):
    """An API Server resource.

    Servers are attached to particular locations() and provide services() for
    supported technologies() via bound ips().
    """

    autoproperties = {
        **APIResource.autoproperties,
        'hostname': str,
        'load': int,
        'name': str,
        'station': NordAPIIPV4,
        'status': str,
    }

    def __init__(self, raw: str):
        """
        Args:
            raw: the raw JSON string from which to create the object.
        """
        super().__init__(raw)
        self.add_resource_collection('groups', [Server.Group(graw) for graw in self.raw['groups']])
        self.add_resource_collection('ips', [Server.IPAssignment(ipraw) for ipraw in self.raw['ips']])
        self.add_resource_collection('locations', [Server.Location(lraw) for lraw in self.raw['locations']])
        self.add_resource_collection('services', [Server.Service(sraw) for sraw in self.raw['services']])
        self.add_resource_collection('specifications', [Server.Specification(sraw) for sraw in self.raw['specifications']])
        self.add_resource_collection('technologies', [Server.Technology(traw) for traw in self.raw['technologies']])

    @property
    def country(self) -> Country:
        """The primary (first) Country element of this server.

        Almost always, this corresponds to the server's location.
        """
        return self.locations[0].country

    @staticmethod
    def gen_request_list(filters, limit):
        for filter_key in filters.keys():
            if filter_key not in self.FILTERS:
                raise ValueError(f'Filter key {filter_key} is not a valid filter key.')

    def is_online(self) -> bool:
        """Query the server object's *online* status.

        Returns:
            True, if the server reports any valid *online* status; False,
            otherwise.
        """
        if self.status in ('online'):
            return True
        else:
            return False


    class Group(APIResource):
        """An element of the :code:`Server[groups]` JSON structure."""

        autoproperties = {
            **APIResource.autoproperties,
            'title': str,
            'identifier': str,
        }

        def __init__(self, raw):
            super().__init__(raw)
            self.type = Server.Group.Type(self.raw['type'])


        class Type(APIResource):
            """An element of the :code:`Server[groups][*][type]` JSON structure."""

            autoproperties = {
                **APIResource.autoproperties,
                'title': str,
                'identifier': str,
            }


    class IPAssignment(APIResource):
        """An element of the :code:`Server[ips]` JSON structure."""

        autoproperties = {
            **APIResource.autoproperties,
            'ip_id': int,
            'server_id': int,
            'type': str,
        }

        def __init__(self, raw: str):
            """
            Args:
                raw: the raw JSON string from which to create the object.
            """
            super().__init__(raw)
            self.ip = Server.IPAssignment.IP(self.raw['ip'])


        class IP(APIResource):
            """An element of the :code:`Server[ips][*][ip]` JSON structure."""

            autoproperties = {
                'id': int,
                'ip': NordAPIIPV4,
                'version': int,
            }


    class Location(APIResource):
        """An element of the :code:`Server[locations]` JSON structure."""

        autoproperties = {
            **APIResource.autoproperties,
            'latitude': float,
            'longitude': float,
        }

        def __init__(self, raw: str):
            """
            Args:
                raw: the raw JSON string from which to create the object.
            """
            super().__init__(raw)
            self.country = Server.Location.Country(self.raw['country'])


        class Country(APIResource):
            """An element of the :code:`Server[locations][*][country]` JSON structure.
            """

            autoproperties = {
                'id': int,
                'name': str,
                'code': str,
            }

            def __init__(self, raw: str):
                """
                Args:
                    raw: the raw JSON string from which to create the object.
                """
                super().__init__(raw)
                self.city = City(self.raw['city'])


    class Service(APIResource):
        """An element of the :code:`Server[services]` JSON structure."""

        autoproperties = {
            **APIResource.autoproperties,
            'name': str,
            'identifier': str,
        }


    class Specification(APIResource):
        """An element of the :code:`Server[specifications]` JSON structure.

        Note: the only Specification indeitifer I have yet seen is 'Version'.
        """

        autoproperties = {
            'id': int,
            'identifier': str,
            'title': str,
        }

        def __init__(self, raw: str):
            """
            Args:
                raw: the raw JSON string from which to create the object.
            """
            super().__init__(raw)
            self.values = [Server.Specification.Value(kvp) for \
                           kvp in raw['values']]


        class Value(APIResource):
            """An element of the :code:`Server[specifications][*][values]` JSON
            structure.
            """

            autoproperties = {
                'id': int,
                'value': str,
            }


    class Technology(APIResource):
        """An element of the :code:`Server[technologies]` JSON structure.

        Attributes:
            pivot (Server.Technology.Pivot): a reference to this Technology
                instance's parent Server object.
            metadata (list[Server.Technology.Metadata]): a list of metadata
                key-value pairs for this Technology instance.
        """

        autoproperties = {
            **APIResource.autoproperties,
            'name': str,
            'identifier': str,
        }

        def __init__(self, raw: str):
            """
            Args:
                raw: the raw JSON string from which to create the object.
            """
            super().__init__(raw)
            self.pivot = Server.Technology.Pivot(raw = self.raw['pivot'])
            self.metadata = [Server.Technology.Metadata(mraw) for \
                             mraw in self.raw['metadata']]

        def is_online(self) -> bool:
            """Query the *online* status of this Technology's owning Server.

            Returns:
                True; if the owning server is_online(); False, otherwise.
            """
            return self.pivot.is_online()


        class Metadata(APIResource):
            """An element of the :code:`Server[technologies][*][metadata]` JSON
            structure.
            """

            autoproperties = {
                'name': str,
                'value': str,
            }


        class Pivot(APIResource):
            """An element of the :code:`Server[technologies][*][pivot]` JSON
            structure.
            """

            autoproperties = {
                'server_id': int,
                'status': str,
                'technology_id': int,
            }


class Technology(APIResource):
    """A known technology type to the NordVPN API.

    Found in the :code:`/technologies` REST endpoint response.
    """

    autoproperties = {
        **APIResource.autoproperties,
        'identifier': str,
        'name': str,
    }
