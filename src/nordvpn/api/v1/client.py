"""A native python client for mangaging connections to the NordVPN REST server.

Typical Usage:

.. code-block:: python

    import nordvpn.api.v1 as nordapi
    client = nordapi.Client()
    online_servers = client.servers()
"""

import json
import logging
import requests

from nordvpn.api.v1.resources import Technology, Server, Country

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


class Client():
    """API Client object for NordVPN Public API v1

    This API Client object contains session information and methods for interacting
    with the NordVPN public REST API.

    Attributes:
        api_base (str): the full URL of the API endpoint in use
    """

    API_ENDPOINT = 'v1'
    """the string path below the host, where the API begins."""

    def __init__(self, host: str='api.nordvpn.com', scheme: str='https'):
        """Create a new instance of Client.

        Args:
            host: the URI of the API host; normally api.nordvpn.com
            scheme: the HTTP scheme to use in server communications
        """
        self.api_base = '{scheme}://{host}/{self.API_ENDPOINT}'.format(
            **locals()
        )
        self.default_limit=5

    def countries(self) -> list[Country]:
        """Get a List of all Country resources known to the NordVPN API.

        Queries the NordVPN '/countries' endpoint.

        Returns:
            A List of digested Country APIResources.
        """
        return [Country(cj) for cj in self._api_get('countries')]


    def groups(self) -> list[Server.Group]:
        """Get a List of all active NordVPN server groups.

        Queries the NordVPN '/servers/groups' endpoint.

        Returns:
            A List of digested Server.Group APIResources.
        """
        groups_raw = self._api_get('servers', 'groups')
        return [Server.Group(raw=gr) for gr in groups_raw]

    def technologies(self) -> list[Technology]:
        """Get a List of the technologies recognized by the NordVPN API.

        Queries the NordVPN '/technologies' endpoint.

        Returns:
            A List of digested Technology APIResources.
        """
        techs_raw = self._api_get('technologies')
        return [Technology(raw=tr) for tr in techs_raw]

    def recommendations(self, resource_filters=[]) -> list[Server]:
        """Get a List of recommended servers based on resource_filters.

        Queries the NordVPN '/servers' endpoint with the resource_filters
        applied as selectors.

        Returns:
            A List of digested Server APIResources.
        """
        logger.debug('resource_filters=%s' % resource_filters)

        recs_raw = self._api_get('servers', filters=resource_filters)
        recs = [Server(rr) for rr in recs_raw]
        return recs

        params = {}
        for filter_name, filter_value in rec_filters.items():
            if filter_value is None:
                continue
            params[f'filters[{filter_name}]'] = int(filter_value)

        session = requests.Session()
        session.mount(self.api_base, TestAdapter())
        req = requests.Request(method='GET', url=self.api_base)
        prep = session.prepare_request(req)
        prep.url = self.api_base + '/servers/recommendations?' + '&'.join(
            [key + '=' + str(value) for key,value in params.items()]
        )
        print(prep.url)
        print(type(prep.url))
        recommendations_raw = session.send(prep)
        rj = recommendations_raw.json()

    def server_countries(self) -> list[Country]:
        """Get a List of all active countries that NordVPN is serving.

        Queries the NordVPN '/servers/countries' endpoint.

        Returns:
            A List of digested Country APIResources.
        """
        return [Country(raw=cj) for cj in self._api_get('servers', 'countries')]

    def servers(self, resource_filters=None) -> list[Server]:
        """Get a List of all active NordVPN servers.

        Queries the NordVPN '/servers' endpoint.

        Returns:
            A List of digested Server APIResources.
        """
        servers_raw = self._api_get('servers')
        return [Server(raw=sj) for sj in servers_raw]

    def _api_get(self, *elements, filters=[], **kwargs):
        """GET an arbitrary REST resource from the NordVPN public API.

        Send an HTTP request to the Client's connected API endpoint, built from
        the *elements collection and applying 'filters' options.

        Args:
            *elements: the component elements of the resource (URL) path
            filters: Optional; collection of resource filters to apply to the
                request.
            **kwargs: Optional; additional keyword arguments, passed-through to
                the backend HTTP .get() method.

        Returns:
            A JSON-formatted string of the response body.
        """
        url = '/'.join([self.api_base, *elements])
        filter_string = '&'.join([repr(f) for f in filters])
        if len(filter_string) > 0:
            url += f'?{filter_string}'

        resp = self._http_get(url, **kwargs)
        return resp.json()

    def _http_get(self, url, **kwargs):
        """Send an arbitrary HTTP GET request."""
        resp = requests.get(url, **kwargs)
        logger.debug(resp.url)
        if not resp.ok:
            logger.warning('HTTP Error %d:%s' % (resp.status_code, resp.reason))
            logger.debug(resp.headers)
            logger.debug(resp.text)
        return resp

    def __str__(self):
        return self.api_base
