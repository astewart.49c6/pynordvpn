"""A Click-based Command Line Interface for the nordvpn module.

This CLI provides JSON, GREP-able, and pretty-formatting output for many of the
operations exposed by the :code:`api` modules.

Using the CLI from the shell:

.. code-block:: bash

    python3 cli.py --debug servers
    python3 cli.py --output posix recommend
    python3 cli.py --help
"""

import json
import logging
import sys

import click

import nordvpn.api.v1 as api

logger = logging.getLogger(__name__)


#: Country Blacklists
CN_BLACKLISTS = {
    'five_eyes': set(['au', 'ca', 'gb', 'nz', 'uk', 'us']),
    'nine_eyes': set(['au', 'ca', 'nz', 'uk', 'us', 'dk', 'fr', 'nl', 'no']),
    'fourteen_eyes': set(['au', 'ca', 'nz', 'uk', 'us', 'dk', 'fr', 'nl', 'no',
                     'be', 'de', 'it', 'es', 'se']),
}

ERROR_CODES = {
    'OK': (0, 'All operations succeeded'),
    'FAIL': (1, 'A generic runtime error occurred.'),
    'API_SERVER_ERROR': (129,
        'The API request was understood, but the server is not responding or \
        returned an unhandled HTTP error.'),
    'API_EMPTY': (130,
        'The API request was understood and the server returned a response, \
        but it contained no objects.'),
}

CONTEXT_SETTINGS = {
    'help_option_names': ['-h', '--help'],
}
CTX_CLIENT = f'{__name__}.client'
CTX_OUTPUT_FORMAT = f'{__name__}.output_format'
CTX_OUTPUT = f'{__name__}.debug'
OUTPUT_FORMATS = [
    'posix', 'json', 'pretty'
]

@click.group(context_settings=CONTEXT_SETTINGS)
@click.pass_context
@click.option('-o', '--output', type=click.Choice(OUTPUT_FORMATS,
              case_sensitive=False), default='pretty')
@click.option('-d', '--debug', is_flag=True, default=False)
def cli_main(ctx, debug, output):
    """The canonical entrypoint for the Click CLI.

    During execution of this entrypoint, the Click Context object's metadata is
    populated with an API Client corresponding to the REST server's API version.
    The context Client object is passed to the subsequent command execution
    method.
    """
    ctx.meta[CTX_CLIENT] = api.Client(scheme='https')
    ctx.meta[CTX_OUTPUT_FORMAT] = output
    if debug:
        logging.basicConfig(level=logging.DEBUG)

@cli_main.command()
@click.pass_context
def cities(ctx):
    """Fetch and print the list of active cities.

    Gets a list of active City resources based on the 'server_countries' Client
    operation. Prints a formatted table of cities to stdout.
    """
    _countries = ctx.meta[CTX_CLIENT].server_countries()
    _cities = []
    for country in _countries:
        for city in country.cities:
            city.country_id = country.id
            city.country_code = country.code
            _cities.append(city)

    output_format = ctx.meta[CTX_OUTPUT_FORMAT]
    if output_format == 'posix':
        for city in sorted(_cities, key=lambda c: c.id):
            sys.stdout.write(
                '{id} "{name}" {latitude} {longitude} {dns_name}\n'.format(
                    **city.__dict__))
    elif output_format == 'json':
        raise NotImplementedError()
    elif output_format == 'pretty':
        column_format = \
            '{id:7} {name:20} {latitude:12} {longitude:12} {dns_name}\n'
        headers = {
            'dns_name': 'DNS_NAME',
            'id': 'ID',
            'latitude': 'LATITUDE',
            'longitude': 'LONGITUDE',
            'name':'NAME',
            }

        sys.stdout.write(column_format.format(**headers))
        for city in sorted(_cities, key=lambda c: c.id):
            sys.stdout.write(column_format.format(**city.__dict__))

@cli_main.command()
@click.pass_context
def countries(ctx):
    """Fetch and print a listing of Countries known to the API.

    Args:
        ctx (click.Context): The Click Context for this execution.
    """
    countries = ctx.meta[CTX_CLIENT].countries()

    format = ctx.meta[CTX_OUTPUT_FORMAT]
    if format == 'posix':
        for country in sorted(countries, key=lambda c: c.id):
            sys.stdout.write('{id} {code} {name}\n'.format(**country.__dict__))
    elif format == 'json':
        raise NotImplementedError()
    elif format == 'pretty':
        column_format = '{id:3} {code:4} {name}\n'
        headers = {
            'code': 'CODE',
            'id': 'ID',
            'name':'NAME',
            }

        sys.stdout.write(column_format.format(**headers))
        for country in sorted(countries, key=lambda c: c.id):
            sys.stdout.write(column_format.format(**country.__dict__))

@cli_main.command()
@click.pass_context
@click.option('-t', '--technology',
    help='A technology name or ID to which recommendations will be limited')
@click.option('-g', '--group',
    help='A server group name or ID to which recommendations will be limited')
@click.option('-c', '--country',
    help='A country name or ID to which recommendations will be limited')
@click.option('-a', '--all-countries', is_flag=True, default=False,
    help='Skip excluding recommendations based on surveillance agreements.')
@click.option('-s', '--sort-field', default='load',
    help='The Server APIResource attribute by which the recommendations output \
          will be sorted.')
@click.option('--five-eyes/--no-five-eyes', default=False,
    help='Allow "Five Eyes" agreement countries in the final recommendation; \
          else, exclude them.')
@click.option('--nine-eyes/--no-nine-eyes', default=False,
    help='Allow "Nine Eyes" agreement countries in the final recommendation; \
          else, exclude them.')
@click.option('--fourteen-eyes/--no-fourteen-eyes', default=False,
    help='Allow "Fourteen Eyes" agreement countries in the final \
          recommendation; else, exclude them.')
@click.option('-n', 'number_results', default=1,
    help='The natural number of recommendations which will be printed to \
          stdout.')
def recommend(ctx, all_countries, country, five_eyes, fourteen_eyes, group,
              nine_eyes, number_results, sort_field, technology):
    """Use resource filtering to recommend a list of secure servers.

    Fetches a list of recommended servers using the REST API's 'recommendations'
    operation and resource filters supplied by CLI options.

    (Optionally) Narrow the recommendations list to exclude certain
    international surveillance agreements.
    """
    client = ctx.meta[CTX_CLIENT]

    filter_tech = None
    filter_group = None
    filter_country = None

    # Interpret the 'technology' option as an API Resource Filter
    if technology:
        techs = client.technologies()
        tech = search_techs(technology, techs)
        filter_tech = api.ResourceFilter.from_APIResource(tech)

    # API: Request server recommendations from the API server.
    recommendations = client.recommendations(resource_filters=[filter_tech])
    logger.info('client returned %d recommendations (before filtering)' %
                 len(recommendations))

    # exit early if the recommendations are blank
    if len(recommendations) == 0:
        logger.error("""\
No recommendations were returned by the nordvpn api. Retry the request or use
the 'servers' command to access the server lists directly.
""")
        sys.exit(ERROR_CODES['API_EMPTY'][0])

    blacklist = set()
    if not all_countries:
        # for each blacklist, check if it is disabled in the command options
        # (eg. --nine-eyes). Unless the blacklist is disabled, add it to the
        # working blacklist set, for later filtering.
        for bl_name, bl_set in CN_BLACKLISTS.items():
            if locals().get(bl_name):
                continue
            blacklist = blacklist.union(bl_set)

    logger.debug('blacklist = %s' % blacklist)

    # Filter the recommendations based on the countries blacklist.
    real_recs = []
    for rec in recommendations:
        if not rec.is_online():
            logger.info(
                'Discarding recommended server %s because it is offline.' %
                    rec.hostname)
            continue
        if rec.country.code.lower() in blacklist:
            logger.info(
                'Discarding recommended server %s for blacklist violation.' %
                    rec.hostname)
            continue
        real_recs.append(rec)
    if len(real_recs) == 0:
        logger.error("""\
Recommendations were returned by the API server, but all were discarded due to
country blacklist violations. You might disable one or more country blacklists
or use the 'servers' command to access the server lists directly.
""")
        sys.exit(ERROR_CODES['API_EMPTY'][0])

    real_recs = apply_sort_field(real_recs, sort_field)
    print_server_list(real_recs[0:number_results], ctx.meta[CTX_OUTPUT_FORMAT])

@cli_main.command()
@click.pass_context
def groups(ctx):
    groups = ctx.meta[CTX_CLIENT].groups()

    output_format = ctx.meta[CTX_OUTPUT_FORMAT]
    if output_format == 'posix':
        for group in sorted(groups, key=lambda g: g.id):
            sys.stdout.write(
                '{0.id:n} {0.identifier} "{0.title}" {0.type.title}\n'\
                    .format(group))
    elif output_format == 'json':
        raise NotImplementedError()
    elif output_format == 'pretty':
        sys.stdout.write(
            '{:3s} {:36s} {:36s} {:s}\n'\
                .format('ID', 'IDENTIFIER', 'TITLE', 'TYPE (IDENTIFIER)'))
        for tech in sorted(groups, key=lambda t: t.id):
            sys.stdout.write(
                '{0.id:>3d} {0.identifier:36s} {0.title:36s} {0.type.title} \
                ({0.type.identifier})\n'\
                    .format(tech))

@cli_main.command()
@click.pass_context
@click.option('-s', '--sort-field', default='id')
def servers(ctx, sort_field):
    servers = ctx.meta[CTX_CLIENT].servers()
    servers = apply_sort_field(servers, sort_field)
    print_server_list(servers, ctx.meta[CTX_OUTPUT_FORMAT])

@cli_main.command()
@click.pass_context
def technologies(ctx):
    techs = ctx.meta[CTX_CLIENT].technologies()

    output_format = ctx.meta[CTX_OUTPUT_FORMAT]
    if output_format == 'posix':
        for tech in sorted(techs, key=lambda t: t.id):
            sys.stdout.write(\
                '{0.id:n} {0.identifier} "{0.name}"\n'.format(tech))
    elif output_format == 'json':
        sys.stdout.write(json.dumps([t.raw for t in techs]) + '\n')
    elif output_format == 'pretty':
        sys.stdout.write(
            '{:3s} {:24s} {:s}\n'.format('ID', 'IDENTIFIER', 'NAME'))
        for tech in sorted(techs, key=lambda t: t.id):
            sys.stdout.write(
                '{0.id:>3d} {0.identifier:24s} {0.name:s}\n'.format(tech))

def apply_sort_field(api_resources, sort_field):
    common_properties = set()
    for resource in api_resources:
        common_properties.update(resource.property_names)

    # apply the sort field
    if sort_field not in common_properties:
        raise click.BadOptionUsage('sort-field',
            'Invalid sort field value: %s.\nValid fields are: %s' % \
                (sort_field, ', '.join(sorted(list(common_properties)))))
    return sorted(api_resources, key=lambda r: getattr(r, sort_field))

def print_server_list(servers, output_format):
    if output_format == 'posix':
        for server in servers:
            sys.stdout.write(
                '{0.id:n} {0.hostname} {0.station} {0.load}\n'.format(server))
    elif output_format == 'json':
        raise NotImplementedError()
    elif output_format == 'pretty':
        # header
        sys.stdout.write('{:6} {:32} {:15} {:4} {}\n'.format(
                         'ID', 'HOSTNAME', 'STATION', 'LOAD', 'NAME'))
        # content
        for server in servers:
            sys.stdout.write(
                '{0.id:>6d} {0.hostname:32s} {0.station.compressed:15s} {1:>4} '
                '{0.name}\n'.format(
                    server, server.load if server.is_online() else 'OFF'))
    else:
        raise ValueError('Unknown print format: %s' % format)

def search_techs(search_string, technologies):
    result = None
    for tech in technologies:
        try:
            if tech.id == int(search_string):
                return tech
        except ValueError:
            pass
        if tech.identifier == str(search_string):
            return tech
        elif search_string.lower() in \
            getattr(tech, 'name', '').lower().replace(' ',''):
            return tech
    raise ValueError(
        'Technology search string %s not found in NordVPN technologies.' \
            % search_string)


if __name__ == "__main__":
    cli_main()
