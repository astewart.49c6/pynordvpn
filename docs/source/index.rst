.. pynordvpn documentation master file, created by
   sphinx-quickstart on Sun Dec 20 09:46:59 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. toctree::
   :maxdepth: 3
   :caption: pythonic API v1

   cli
   api/api_v1
   api/resources_v1

Welcome to pynordvpn's documentation!
=====================================


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
