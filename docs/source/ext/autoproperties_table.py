from docutils import nodes
#from docutils.parsers.rst import Directive
from sphinx.util.docutils import SphinxDirective


class AutopropertiesTable(SphinxDirective):

    def run(self):
        """Create a Autoproperties table from an APIResource object.

        | Autoproperty | Type |
        |--------------+------|
        |     blah     | int  |
        """
        test = {
            'foo': 'bar',
            'fizz': 'buzz',
        }

        # create a table
        ap_table = nodes.table()
        ap_table['classes'] += ['colwidths-auto']

        # create table group with 2 columns
        tgroup = nodes.tgroup(cols=2)
        for _ in range(2):
            colspec = nodes.colspec(colwidth=1)
            tgroup.append(colspec)

        # create and add header row
        header = nodes.row()
        for title in ['Autoproperties', 'Type']:
            header += nodes.entry('', nodes.paragraph(text=title))
        tgroup += nodes.thead('', header)

        # creat a table body
        tbody = nodes.tbody()
        # populate the table body with entries
        for apname, aptype in test.items():
            row = nodes.row()

            row += nodes.entry('', nodes.paragraph('', text=str(apname)))
            row += nodes.entry('', nodes.paragraph('', text=str(aptype)))

            tbody += row

        tgroup += tbody

        ap_table += tgroup
        return [ap_table]


def setup(app):
    app.add_directive("autoproperties_table", AutopropertiesTable)

    return {
        'version': '0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True,
    }
