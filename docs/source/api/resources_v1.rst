=========
Resources
=========

API Resources
=============

.. autosummary::

	nordvpn.api.v1.APIResource
	nordvpn.api.v1.City
	nordvpn.api.v1.Country
	nordvpn.api.v1.Server
	nordvpn.api.v1.Technology

.. autoclass:: nordvpn.api.v1.APIResource
   :members:

.. autoclass:: nordvpn.api.v1.City

	.. autosummary:: nordvpn.api.v1.City.autoproperties

.. autoclass:: nordvpn.api.v1.Country

.. autoclass:: nordvpn.api.v1.Server

.. autoclass:: nordvpn.api.v1.Technology

	.. warning::
		The **nordvpn.api.v1.Technology** class is related, but independent from the **nordvpn.api.v1.Server.Technology** class.

		The :code:`Server.Technology.pivot.technology_id` corresponds to the :code:`Technology.id`.


API Primitives
==============

.. autoclass:: nordvpn.api.v1.NordAPIDateTime

.. autoclass:: nordvpn.api.v1.NordAPIInt

.. autoclass:: nordvpn.api.v1.NordAPIIPV4
