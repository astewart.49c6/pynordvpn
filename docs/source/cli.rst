===
CLI
===

.. automodule:: nordvpn.cli
	:no-members:
	:members: CN_BLACKLISTS
	:undoc-members: ERROR_CODES

.. click:: nordvpn.cli:cli_main
	:prog: pynordvpn
	:nested: full
