# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------
import os
import sys

PROJECT_ROOT = os.path.dirname(__file__) + '/../..'
SRC_ROOT = os.path.join(PROJECT_ROOT, 'src')

sys.path.insert(0, os.path.abspath(SRC_ROOT))


# -- Project information -----------------------------------------------------

from setuptools.config import read_configuration

st_config = read_configuration(PROJECT_ROOT + '/setup.cfg')


project = st_config['metadata']['name']
copyright = '2020, ' + st_config['metadata']['author']
author = st_config['metadata']['author']

# The full version, including alpha/beta/rc tags
release = st_config['metadata']['version']


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = []

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'

html_static_path = ['_static']


# -- Autodoc configuration ---------------------------------------------------

extensions += [
    'sphinx.ext.autodoc',
    'sphinx.ext.autosummary',
]

autodoc_mock_imports = []
autodoc_mock_imports.extend(st_config['options']['install_requires'])
autodoc_mock_imports.extend(st_config['options']['extras_require']['cli'])
print(autodoc_mock_imports)

autoclass_content = 'both'
autodoc_default_options = {
    'members': True,
    'undoc-members': True,
}


# -- Napoleon configuration ---------------------------------------------------

extensions += ['sphinx.ext.napoleon']
# The sphinx_autodoc typehints extension must be loaded *after* Napoleon
# https://github.com/agronholm/sphinx-autodoc-typehints/issues/15
extensions += ['sphinx_autodoc_typehints']

napoleon_google_docstring = True
napoleon_numpy_docstring = False
napoleon_use_param = True


# -- Sphinx-click configuration -----------------------------------------------
extensions += ['sphinx_click']


# -- Custom Extentions --------------------------------------------------------
sys.path.append(os.path.abspath(PROJECT_ROOT + '/docs/source/ext'))
extensions += ['autoproperties_table']
