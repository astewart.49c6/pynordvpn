import re
import os.path
from setuptools import setuptools
from setuptools.config import read_configuration

BUILD_ROOT = os.path.dirname(__file__)

conf_dict = read_configuration(BUILD_ROOT + '/setup.cfg')

# sanity check the version string
re_version_string = re.compile('^\d+\.\d+$')  # eg. 1.123
__version = conf_dict['metadata']['version']
assert(re_version_string.match(__version)),\
    'Module version string \"%s\" does not match required scheme. (%s)' % \
        (str(__version), re_version_string.pattern)


setuptools.setup(
    command_options={
        'build_sphinx': {
            'project': ('setup.py', conf_dict['metadata']['name']),
            'version': ('setup.py', conf_dict['metadata']['version']),

            'config_dir': ('setup.py', BUILD_ROOT + '/docs/source'),
            'build_dir': ('setup.py', BUILD_ROOT + '/build/docs'),
            'source_dir': ('setup.py', BUILD_ROOT + '/docs/source'),
        },
    },
)
