# directory variables
BUILDDIR = $(abspath ./build)
DOCS = $(abspath ./docs)
SRC = $(abspath ./src)
DISTDIR = $(BUILDDIR)/dist

# host tools
PYTHON ?= python3
PYLINT ?= -pylint \
	--rcfile=./tests/pylintrc \
	--init-hook='import sys; sys.path.insert(0, "$(SRC)")'
SETUP = $(PYTHON) ./setup.py

# project variables
PYNORDVPN_VERSION = $(shell $(PYTHON) -c 'import src.nordvpn; print(src.nordvpn.__version__)')
PYNORDVPN_PKG = pynordvpn-$(PYNORDVPN_VERSION)


.PHONY: all build clean dist html install lint


## START: BUILD TARGETS ##
all: \
	$(BUILDDIR)/lib \
	dist \
	info \
\

build: $(BUILDDIR)/lib

dist: \
	$(DISTDIR)/$(PYNORDVPN_PKG)-py3-none-any.whl \
	$(DISTDIR)/$(PYNORDVPN_PKG).tar.gz \
	$(DISTDIR)/$(PYNORDVPN_PKG)-py39.egg \
\

$(BUILDDIR)/lib:
	$(SETUP) build

$(BUILDDIR)/pynordvpn.dist-info:
	$(SETUP) dist_info
	mv pynordvpn.dist-info $(@D)

$(BUILDDIR)/pynordvpn.egg-info:
	$(SETUP) egg_info
	mv pynordvpn.egg-info $(@D)

## BINARY EGG DISTRIBUTION
$(DISTDIR)/$(PYNORDVPN_PKG)-%.egg:
	$(SETUP) bdist_egg

## BINARY WHEEL DISTRIBUTION
$(DISTDIR)/$(PYNORDVPN_PKG)-%.whl:
	$(SETUP) bdist_wheel

## SOURCE DISTRIBUTION
$(DISTDIR)/$(PYNORDVPN_PKG).tar.gz:
	$(SETUP) sdist
## END: BUILD TARGETS##


## START: DEVELOPMENT TARGETS ##
check: unittest

clean:
	@rm -rf "$(BUILDDIR)"
	@rm -rf "./.pytest_cache"
	@find $(SRC) -type d -name '*.egg-info' -exec rm -r {} +
	@find $(SRC) -type d -name '*.dist-info' -exec rm -r {} +
	@find $(SRC) -type d -name '__pycache__' -exec rm -r {} +
	@find ./tests -type d -name '__pycache__' -exec rm -r {} +

# PATH REQUIREMENTS: sphinx-build, make
html:
	$(SETUP) build_sphinx

# build the python dist-info and egg-info files
info: \
	$()
	$(SETUP) dist_info egg_info

install:
	$(SETUP) install
	pip install pynordvpn[CLI]

uninstall:
	$(PYTHON) -m pip uninstall -y pynordvpn

# ignore the pylint error code for now, since it encode information about
# warnings etc. and is therefore likely to be non-zero
lint:
	$(PYLINT) nordvpn
	$(PYLINT) \
		--init-hook='import sys; sys.path.insert(0, "src/nordvpn/api/v1")' \
		nordvpn.api.v1

unittest:
	bash ./tests/run-unit-tests.sh
## END: DEVELOPMENT TARGETS ##
