from tests.utils import snapshot_server
import pytest

import logging
logger = logging.getLogger(__name__)

import nordvpn.api.v1 as api

from pprint import pprint, pformat


@pytest.fixture(scope='module')
def client(snapshot_server):
    return api.Client(host=snapshot_server, scheme='http')


def test_countries_list(client):
    countries = client.countries()
    assert isinstance(countries, list)
    assert len(countries) == 245
    for country in countries:
        assert isinstance(country, api.Country)

def test_recommendations_list(client):
    recommendations = client.recommendations()
    assert isinstance(recommendations, list)
    assert len(recommendations) == 100
    for recommendation in recommendations:
        assert isinstance(recommendation, api.Server)

def test_server_countries_list(client):
    countries = client.server_countries()
    assert isinstance(countries, list)
    assert len(countries) == 59
    for country in countries:
        assert isinstance(country, api.Country)

def test_server_groups_list(client):
    groups = client.groups()
    assert isinstance(groups, list)
    assert len(groups) == 17
    for group in groups:
        assert isinstance(group, api.Server.Group)

def test_servers_list(client):
    servers = client.servers()
    assert isinstance(servers, list)
    assert len(servers) == 100
    for server in servers:
        assert isinstance(server, api.Server)

def test_technologies(client):
    techs = client.technologies()
    assert isinstance(techs, list)
    assert len(techs) == 21
    for tech in techs:
        assert isinstance(tech, api.Technology)

if __name__ == "__main__":
    import pytest
    pytest.main([__file__])
