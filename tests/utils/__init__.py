import multiprocessing as mp

import logging
logger = logging.getLogger(__name__)

from tests.utils.snapshot_server import app

class SnapshotServerDaemon(mp.Process):

    def __init__(self, port=5000):
        super().__init__(target=app.run, kwargs={
            'debug': False,
            'use_reloader': False,
        })

import pytest

from time import sleep
@pytest.fixture(scope='session')
def snapshot_server():
    daemon_process = SnapshotServerDaemon()
    try:
        logger.info('Starting API test server...')
        daemon_process.start()
        # sleep a couple seconds to give the flask server time to wakeup
        sleep(2)
        yield 'localhost:5000'
    finally:
        logger.info('Stopping API test server...')
        daemon_process.terminate()
        daemon_process.join(20)

    if daemon_process.exitcode is None:
        daemon_process.kill()
        daemon_process.join(30)
