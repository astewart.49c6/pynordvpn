from flask import Flask

import tests.utils.api_v1 as api_v1

app = Flask(__name__)

# API V1 Blueprint
app.register_blueprint(api_v1.bp)


if __name__ == "__main__":
    app.run(debug=True)
