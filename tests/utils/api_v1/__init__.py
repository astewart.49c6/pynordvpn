import json
import os.path
from pathlib import Path
from flask import Blueprint, Response

bp = Blueprint('api_v1', __name__, url_prefix='/v1')

def emit_json(path):
    api_base = Path(os.path.dirname(__file__))
    with open(api_base / path, 'r') as fp_resource:
        return Response(
            fp_resource.read(),
            mimetype='application/json',
        )

@bp.route('/countries')
def countries():
    return emit_json('countries.json')

@bp.route('/servers')
def servers():
    return emit_json('servers.json')

@bp.route('/servers/countries')
def servers_countries():
    return emit_json('servers_countries.json')

@bp.route('/servers/groups')
def servers_groups():
    return emit_json('servers_groups.json')

@bp.route('/technologies')
def technologies():
    return emit_json('technologies.json')

@bp.route('/recommendations')
def recommendations():
    return emit_json('recommendations.json')
