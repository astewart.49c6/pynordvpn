#!/bin/bash
# This script trivially runs the unittest entrypoint in :tests/unit/__main__.py,
# with the Coverage.py plugin enabled, and records the unit-test output and
# coverage report under :build/tests/*.
#
# This script accepts no command line arguments, but the output directory can be
# changed by asserting the $BUILDDIR environment variable.
set -euxo pipefail

SCRIPT_ROOT=$(dirname $0)
PROJECT_ROOT=$(realpath "${SCRIPT_ROOT}/..")
TESTS_OUTPUT="${BUILDDIR:-$PROJECT_ROOT/build}/tests/integration"

mkdir -p "${PROJECT_ROOT}/build/tests"

#PYTHONPATH="${PROJECT_ROOT}" \
	#python3 -m tests.integration.test_client_v1 \
PYTHONPATH="${PROJECT_ROOT}" \
	pytest \
		-o log_cli=TRUE \
		--log-cli-level=INFO \
		--junit-xml="${TESTS_OUTPUT}/integration-tests.xml" \
		"${PROJECT_ROOT}/tests/integration"
