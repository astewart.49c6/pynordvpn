#!/bin/bash
# This script trivially runs the unittest entrypoint in :tests/unit/__main__.py,
# with the Coverage.py plugin enabled, and records the unit-test output and
# coverage report under :build/tests/*.
#
# This script accepts no command line arguments, but the output directory can be
# changed by asserting the $BUILDDIR environment variable.
set -euxo pipefail

SCRIPT_ROOT=$(dirname $0)
PROJECT_ROOT=$(realpath "${SCRIPT_ROOT}/..")
TESTS_OUTPUT="${BUILDDIR:-$PROJECT_ROOT/build}/tests/unit"

mkdir -p "${PROJECT_ROOT}/build/tests"

PYTHONPATH="${PROJECT_ROOT}/src:${PROJECT_ROOT}" \
	python3 -m tests.unit \
	--junit-xml="${TESTS_OUTPUT}/unit-tests.xml" \
	--cov=${PROJECT_ROOT}/src/nordvpn \
	--cov-report html:${TESTS_OUTPUT}/coverage \
	--cov-report term \
	--cov-config=<(cat <<EOF
[run]
branch = True
data_file = ${TESTS_OUTPUT}/coverage/.coverage
EOF
)
