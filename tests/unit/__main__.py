#!/usr/bin/env python3

import os
import sys
SCRIPT_ROOT = os.path.dirname(__file__)
# add the :tests/unit directory to PYTHONPATH
sys.path.insert(0, os.path.realpath(SCRIPT_ROOT))


import logging
logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)

import pytest
sys.exit(
    pytest.main(sys.argv[1:] + ['--verbosity=1'] + [SCRIPT_ROOT])
    )
