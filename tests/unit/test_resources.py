import json
import os.path
from pytest import raises
from abc import abstractmethod
from pprint import pprint, pformat

import nordvpn.api.v1 as api

DIR_TESTS = os.path.dirname(__file__)
DIR_SNAPSHOTS = os.path.join(DIR_TESTS, '../utils/api_v1')

class Test_NordAPIDateTime():

    def test_init(self):
        dt = api.NordAPIDateTime('2001-11-13 12:12:12')
        assert isinstance(dt, api.NordAPIDateTime)


class Test_APIResource():

    resource_cls = api.APIResource
    default_raw = {
        'id': 120,
        'created_at': '1990-01-01 20:18:08',
        'updated_at': '1990-01-02 20:18:16',
        }
    repr_snapshot = 'APIResource(created_at="1990-01-01 20:18:08", id="120", updated_at="1990-01-02 20:18:16")'

    def test_init(self):
        res = self.resource_cls(raw = self.default_raw)
        assert res.raw != None
        with raises(ValueError): self.resource_cls({})
        with raises(ValueError): self.resource_cls(None)
        with raises(ValueError): self.resource_cls([self.default_raw])

    def test_autoproperties(self):
        autoprops = self.resource_cls.autoproperties.copy()
        ap_values = {}
        ap_values2 = {}
        for prop, cast in autoprops.items():
            if cast == int:
                ap_values[prop] = int(123)
                ap_values2[prop] = int(321)
            elif cast == str:
                ap_values[prop] = 'foobar'
                ap_values2[prop] = 'barfoo'
            elif cast == float:
                ap_values[prop] = 12.3
                ap_values2[prop] = 32.1
            elif cast == api.NordAPIDateTime:
                ap_values[prop] = api.NordAPIDateTime('2020-02-20 10:18:20')
                ap_values2[prop] = api.NordAPIDateTime('1998-08-19 03:30:14')
            elif cast == api.NordAPIIPV4:
                ap_values[prop] = api.NordAPIIPV4('1.1.1.1')
                ap_values2[prop] = api.NordAPIIPV4('254.254.254.254')
            else:
                raise NotImplementedError('cast value %s is not impelemnted in test suite.' % cast)

        res = self.resource_cls({**self.default_raw.copy(), **ap_values})

        for prop, cast in autoprops.items():
            assert getattr(res, prop) == ap_values[prop]

            # changing a property value
            res.__setattr__(prop, ap_values2[prop])
            assert getattr(res, prop) == ap_values2[prop]

        # change an attribute which is not an autoproperty
        res.__setattr__('BadValue', 'foobar')
        assert res.BadValue == 'foobar'

    @abstractmethod
    def test_children(self):
        pass

    def test_properties(self):
        """All keys from the public API should either be represented by an autoproperty, or by an APIResource object..
        """
        res = self.resource_cls(raw=self.default_raw)
        for key in res.raw.keys():
            assert str(key) in res.property_names

    def test_repr(self):
        res = self.resource_cls(raw=self.default_raw)
        assert repr(res) == self.repr_snapshot


class Test_Server(Test_APIResource):
    resource_cls = api.Server
    # use the uk1780 server, because it has Wireguard_udp resources, which has
    # populated metadata.
    default_raw = json.load(open(DIR_SNAPSHOTS + '/servers.json', 'r'))[2]
    repr_snapshot = 'Server(created_at="2017-01-24 00:00:00", hostname="uk1780.nordvpn.com", id="727881", load="22", name="United Kingdom #1780", station="185.38.150.91", status="online", updated_at="2020-09-12 21:33:46")'

    def test_children(self):
        """Check the types of each child resource reference and, where reasonable, verify that it has been populated correctly.
        """
        res = self.resource_cls(raw=self.default_raw)

        assert isinstance(res.groups, list)
        assert len(res.groups) == 2

        assert isinstance(res.locations, list)
        assert len(res.locations) == 1

        assert isinstance(res.services, list)
        assert len(res.services) == 2

        assert isinstance(res.technologies, list)
        assert len(res.technologies) == 6

        assert isinstance(res.ips, list)
        assert len(res.ips) == 1
