# Contributing

Thanks for contributing to this project!


## Project Information

Canonical Upstream Source: https://gitlab.com/astewart.49c6/pynordvpn

Issue Tracking; https://gitlab.com/astewart.49c6/pynordvpn/-/issues

Current Maintainers:
* Alex Stewart ([@amstewart](https://gitlab.com/astewart.49c6))


## Release/Branch Information

This project follows a mainline branching model. The mainline branch ref is `master`.

This project does not backport changes to prior releases. As a result, stable releases do not have their own product branches.


## Contributing a Pull Request

1. Fork the canonical upstream source repository (ref. `Project Information` above) on GitLab.
  1. Base your changes on the mainline branch ref.
1. Please check that your changes do not cause regressions in the project's testsuite.
    ```bash
    # from the top level directory in the project
    # or with PYTHOPATH=$(git rev-parse --show-toplevel)
    python3 ./tests/run-tests.py
    ```
1. Create a Merge Request on GitLab against the mainline master branch.
