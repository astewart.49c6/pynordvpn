# pynordvpn

 [![pipeline status](https://gitlab.com/astewart.49c6/pynordvpn/badges/master/pipeline.svg)](https://gitlab.com/astewart.49c6/pynordvpn/-/commits/master) [![coverage report](https://gitlab.com/astewart.49c6/pynordvpn/badges/master/coverage.svg)](https://gitlab.com/astewart.49c6/pynordvpn/-/commits/master)

PyNordVPN is a thin, python wrapper to the NordVPN public REST API. This project has absolutely no relationship to the companies which retain rights to the NordVPN service.

**Project Requirements**

This project supports only python runtimes >3.7. This project uses the [requests](https://3.python-requests.org/) python3 module for HTTP interactions, and the [Click](https://click.palletsprojects.com) module for rich CLI interaction.


## Installation

This project is not currently distributed on the [Python Package Index](https://pypi.org/). However, you can quickly install the package via `pip` using one of the methods below.

**1. Magically install the package directly from upstream**

```bash
pip install git+https://gitlab.com/astewart.49c6/pynordvpn@master
```

**2. Build and install using the Makefile**
```bash
git clone https://gitlab.com/astewart.49c6/pynordvpn
cd pynordvpn
make install      # installs to your python site-packages
# make uninstall  # uninstalls from your site
```
See `CONTRIBUTING.md` for more detailed information about building.

The `pynordvpn` package will install
1. a native python module `nordvpn` to the python3 installation prefix
2. a `pynordvpn` wrapper script for interacting with the CLI (usage below) to your `$PATH`.


## Command Line Usage

The command line client uses the `click` module as its framework. Use `--help` to print usage information to stdout.

**Get a list of nordvpn servers, technologies, etc.**
```bash
#pynordvpn {cities | countries | servers | technologies}
> pynordvpn servers
ID     HOSTNAME                         STATION         LOAD NAME
727881 uk1780.nordvpn.com               185.38.150.91      8 United Kingdom #1780
929912 ca944.nordvpn.com                172.83.40.219     29 Canada #944
929966 pl128.nordvpn.com                194.99.105.100    17 Poland #128
930017 nl713.nordvpn.com                185.212.171.68    29 Netherlands #713
930065 dk151.nordvpn.com                82.102.20.236     10 Denmark #151
...

```

**Get a list of recommended servers via NordVPN's recommendation API.**

NOTE: The response from this command is *opinionated*, by default. It will remove server recommendations which are within the fourteen eyes security group, unless you explicitly give it the `-a` CLI argument.
```bash
> pynordvpn recommend
ID     HOSTNAME                         STATION         LOAD NAME
930773 cz94.nordvpn.com                 89.238.186.244     6 Czech Republic #94
```

**Change output format.**

The CLI supports several output formats, to ease parsing results. The default output format is "pretty". It is recommended that you use either the json, or posix output for script parsing.

```bash
pynordvpn -o pretty {servers |...}
pynordvpn -o posix {servers |...}
pynordvpn -o json {servers |...}
```

## Module Usage

```python
>>> import nordvpn.api.v1 as nordapi
>>> client = nordapi.Client()
>>> client.countries()[:3]
[Country(code="AF", id="1", name="Afghanistan"), Country(code="AL", id="2", name="Albania"), Country(code="DZ", id="3", name="Algeria")]

>>> server = client.servers()[0]

>>> server.property_names
{'station', 'groups', 'id', 'locations', 'specifications', 'load', 'updated_at', 'ips', 'status', 'hostname', 'name', 'services', 'created_at', 'technologies'}

>>> server.locations
[Location(country="Country(city="City(dns_name="london", hub_score="0", id="2989907", latitude="51.514125", longitude="-0.093689", name="London")
", code="GB", id="227", name="United Kingdom")
", created_at="2017-06-15 14:06:47", id="45", latitude="51.514125", longitude="-0.093689", updated_at="2017-06-15 14:06:47")]
```
